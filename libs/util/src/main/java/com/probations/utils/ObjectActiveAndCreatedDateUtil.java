package com.barrans.util;

public class ObjectActiveAndCreatedDateUtil {

	public static void registerObject(CommonObjectActiveAndCreatedDate obj) {
		registerObject(obj, 0L);
	}

	public static void registerObject(CommonObjectActiveAndCreatedDate obj, long userId) {
		obj.setActiveAt(DateUtil.now());
		obj.setActive(true);
		obj.setVersion(0L);
		obj.setCreatedBy(userId);
		obj.setCreatedAt(DateUtil.now());
		obj.setUpdatedBy(userId);
		obj.setUpdatedAt(DateUtil.now());
	}

	public static void updateObject(CommonObjectActiveAndCreatedDate obj, boolean isactiveBefore) {
		updateObject(obj, 0L, isactiveBefore);
	}

	public static void updateObject(CommonObjectActiveAndCreatedDate obj, long userId, boolean currentActiveStatus) {
		if (currentActiveStatus != obj.isActive()) {
			if (currentActiveStatus) {
				obj.setActiveAt(null);
				obj.setInactiveDatetime(null);
			} else {
				obj.setInactiveDatetime(DateUtil.now());
			}
		}
		obj.setActive(currentActiveStatus);
		obj.setVersion(obj.getVersion() + 1L);
		obj.setUpdatedBy(userId);
		obj.setUpdatedAt(DateUtil.now());
	}

	public static void registerObject(CommonObjectCreatedDate obj) {
		registerObject(obj, 0L);
	}

	public static void registerObject(CommonObjectCreatedDate obj, long userId) {
		obj.setVersion(0L);
		obj.setCreatedBy(userId);
		obj.setCreatedAt(DateUtil.now());
		obj.setUpdatedBy(userId);
		obj.setUpdatedAt(DateUtil.now());
	}

	public static void updateObject(CommonObjectCreatedDate obj) {
		updateObject(obj, 0L);
	}

	public static void updateObject(CommonObjectCreatedDate obj, long userId) {
		obj.setVersion(obj.getVersion() + 1L);
		obj.setUpdatedBy(userId);
		obj.setUpdatedAt(DateUtil.now());
	}

}
