package com.barrans.util;

import org.hibernate.dialect.PostgreSQL10Dialect;

import com.vladmihalcea.hibernate.type.json.JsonNodeStringType;

import java.sql.Types;

public class CustomPostgreSQLDialect extends PostgreSQL10Dialect {
	public CustomPostgreSQLDialect() {
		super();
		this.registerHibernateType(Types.OTHER, JsonNodeStringType.class.getName());
	}
}
