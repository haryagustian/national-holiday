package com.barrans.util;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@MappedSuperclass
public class CommonObjectActiveAndCreatedDate extends PanacheEntityBase {

	@Column(name = "active_at", nullable = false)
	@NotNull(message = "Please provide a active datetime")
	public LocalDateTime activeAt;

	@Column(name = "inactive_at", nullable = true)
	public LocalDateTime inactiveAt;

	@Column(name = "is_active", nullable = false)
	@NotNull(message = "Please provide a is active")
	public boolean isActive;

	@Column(name = "version", nullable = false)
	@NotNull(message = "Please provide a version")
	public Long version;

	@Column(name = "created_by", nullable = false)
	@NotNull(message = "Please provide a created by")
	public Long createdBy;

	@Column(name = "created_at", nullable = false)
	@NotNull(message = "Please provide a created datetime")
	public LocalDateTime createdAt;

	@Column(name = "updated_by", nullable = false)
	@NotNull(message = "Please provide a updated by")
	public Long updatedBy;

	@Column(name = "updated_at", nullable = false)
	@NotNull(message = "Please provide a updated datetime")
	public LocalDateTime updatedAt;

	public LocalDateTime getInactiveAt() {
		return inactiveAt;
	}

	public void setInactiveDatetime(LocalDateTime inactiveAt) {
		this.inactiveAt = inactiveAt;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public LocalDateTime getActiveAt() {
		return activeAt;
	}

	public void setActiveAt(LocalDateTime activeAt) {
		this.activeAt = activeAt;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

}
