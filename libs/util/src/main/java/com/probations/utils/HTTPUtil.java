package com.barrans.util;
//package com.probations.service.cms.utils;
//
//import java.io.BufferedReader;
//import java.io.InputStreamReader;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//
//import javax.net.ssl.SSLContext;
//
//import org.apache.http.HttpResponse;
//import org.apache.http.NameValuePair;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.config.RequestConfig;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.conn.ssl.NoopHostnameVerifier;
//import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
//import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.HttpClientBuilder;
//import org.apache.http.impl.client.HttpClients;
//import org.apache.http.message.BasicNameValuePair;
//import org.apache.http.ssl.SSLContexts;
//import org.json.simple.JSONObject;
//import org.json.simple.parser.JSONParser;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.fasterxml.jackson.databind.DeserializationFeature;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.google.gson.Gson;
//
//public class HTTPUtil {
//
//	private static final Logger LOGGER = LoggerFactory.getLogger(HTTPUtil.class.getName());
//	private static final int TIMEOUT = 10000;
//
//	public static Object postRequestPushNotif(String url, Object object, String auth) throws Exception {
//		RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(TIMEOUT)
//				.setConnectTimeout(TIMEOUT).setSocketTimeout(TIMEOUT).build();
//		HttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build();
//
//		HttpPost request = new HttpPost("" + url);
//		Gson gson = new Gson();
//		String json = gson.toJson(object);
//		StringEntity params = new StringEntity(json);
//		request.addHeader("Content-Type", "application/json");
//		request.addHeader("Authorization", "Basic " + auth);
//		request.setEntity(params);
//		HttpResponse response = httpClient.execute(request);
//
//		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
//
//		StringBuffer content = new StringBuffer();
//		String line = "";
//		while ((line = rd.readLine()) != null) {
//			content.append(line);
//		}
//		return new JSONParser().parse(content.toString());
//	}
//
//	@SuppressWarnings("rawtypes")
//	public static Object getRequest(String url, Map<String, String> param) throws Exception {
//		HttpClient httpClient = HttpClientBuilder.create().build();
//
//		if (param != null && param.size() > 0) {
//			url += "?";
//			Iterator it = param.entrySet().iterator();
//			while (it.hasNext()) {
//				Map.Entry pair = (Map.Entry) it.next();
//				url += pair.getKey() + "=" + pair.getValue();
//				if (it.hasNext()) {
//					url += "&";
//				}
//			}
//		}
//		LOGGER.info("URI : " + url);
//		HttpGet request = new HttpGet(url);
//		HttpResponse response = httpClient.execute(request);
//		return response.getEntity();
//	}
//
//	@SuppressWarnings("rawtypes")
//	public static Object getRequestCustom(String url, Map<String, String> param) throws Exception {
//		HashMap<String, Object> result = new HashMap<String, Object>();
//		HttpClient httpClient = HttpClientBuilder.create().build();
//
//		if (param != null && param.size() > 0) {
//			url += "?";
//			Iterator it = param.entrySet().iterator();
//			while (it.hasNext()) {
//				Map.Entry pair = (Map.Entry) it.next();
//				url += pair.getKey() + "=" + pair.getValue();
//				if (it.hasNext()) {
//					url += "&";
//				}
//			}
//		}
//		HttpGet request = new HttpGet(url);
//		HttpResponse response = httpClient.execute(request);
//
//		BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
//
//		StringBuffer content = new StringBuffer();
//		String line = "";
//		while ((line = br.readLine()) != null) {
//			content.append(line);
//		}
//
//		result.put("request", url);
//		result.put("response", content.toString());
//
//		return result;
//	}
//
//	public static Object getRequest(String url) throws Exception {
//		RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(TIMEOUT)
//				.setConnectTimeout(TIMEOUT).setSocketTimeout(TIMEOUT).build();
//		HttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build();
//		HttpGet request = new HttpGet("" + url);
//		HttpResponse response = httpClient.execute(request);
//		return response.getEntity();
//	}
//
//	public static Object postRequest(String url, Object object, String customId) throws Exception {
//		RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(TIMEOUT)
//				.setConnectTimeout(TIMEOUT).setSocketTimeout(TIMEOUT).build();
//		HttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build();
//
//		HttpPost request = new HttpPost("" + url);
//		Gson gson = new Gson();
//		String json = gson.toJson(object);
//		StringEntity params = new StringEntity(json);
//		request.addHeader("content-type", "application/json");
//		if (customId != null && !customId.equalsIgnoreCase("")) {
//			request.addHeader("X-Consumer-Custom-ID", customId);
//		}
//		request.setEntity(params);
//		HttpResponse response = httpClient.execute(request);
//
//		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
//
//		StringBuffer content = new StringBuffer();
//		String line = "";
//		while ((line = rd.readLine()) != null) {
//			content.append(line);
//		}
//		ObjectMapper om = new ObjectMapper();
//		om.enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS);
//		return om.readValue(content.toString(), JSONObject.class);
//	}
//
//	public static Object postRequestFormData(String url, Map<String, String> mapRequest) throws Exception {
//		HttpPost request = new HttpPost(url);
//
//		SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, new TrustSelfSignedStrategy()).build();
//
//		RequestConfig.Builder requestBuilder = RequestConfig.custom();
//		requestBuilder.setConnectTimeout(10000);
//		requestBuilder.setConnectionRequestTimeout(10000);
//		requestBuilder.setSocketTimeout(10000);
//
//		SSLConnectionSocketFactory sslConSocFactory = new SSLConnectionSocketFactory(sslContext,
//				new NoopHostnameVerifier());
//		HttpClientBuilder httpClientBuilder = HttpClients.custom();
//		httpClientBuilder.setDefaultRequestConfig(requestBuilder.build());
//
//		List<NameValuePair> params = new ArrayList<NameValuePair>();
//		for (Map.Entry<String, String> entry : mapRequest.entrySet()) {
//			params.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
//		}
//		request.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
//
//		HttpResponse response = httpClientBuilder.setSSLSocketFactory(sslConSocFactory).build().execute(request);
//		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
//
//		StringBuffer content = new StringBuffer();
//		String line = "";
//		while ((line = rd.readLine()) != null) {
//			content.append(line);
//		}
//		LOGGER.info(content.toString());
//		Object objResp = new JSONParser().parse(content.toString());
//		LOGGER.info(objResp.toString());
//		return objResp;
//	}
//
//}
