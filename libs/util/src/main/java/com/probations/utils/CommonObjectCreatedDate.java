package com.barrans.util;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@MappedSuperclass
public class CommonObjectCreatedDate extends PanacheEntityBase {

	@Column(name = "version", nullable = false)
	@NotNull(message = "Please provide a version")
	private Long version;

	@Column(name = "created_by", nullable = false)
	@NotNull(message = "Please provide a created by")
	private Long createdBy;

	@Column(name = "created_at", nullable = false)
	@NotNull(message = "Please provide a created datetime")
	private LocalDateTime createdAt;

	@Column(name = "updated_by", nullable = false)
	@NotNull(message = "Please provide a updated by")
	private Long updatedBy;

	@Column(name = "updated_at", nullable = false)
	@NotNull(message = "Please provide a updated datetime")
	private LocalDateTime updatedAt;

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
