package com.barrans.util;

public interface IAction {
	SimpleResponse insert(Object param, String header);

	SimpleResponse update(Object param, String header);

	SimpleResponse inquiry(Object param);

	SimpleResponse entity(Object param);
}
