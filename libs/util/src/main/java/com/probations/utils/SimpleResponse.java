package com.barrans.util;

public class SimpleResponse {
    private Long status;
    private String message;
    private Object data;

 	public SimpleResponse(Long status, String message, Object data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setContent(Object data) {
		this.data = data;
	}

}