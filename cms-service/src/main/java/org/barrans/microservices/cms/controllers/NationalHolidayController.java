package org.barrans.microservices.cms.controllers;

import com.barrans.util.IAction;
import com.barrans.util.SimpleResponse;
import org.barrans.microservices.cms.services.NationalHolidayService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/api/v1/hrd/national-holiday")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class NationalHolidayController implements IAction {

  @Inject
  NationalHolidayService nationalHolidayService;

  @Override
  @POST
  @Path("/insert")
  public SimpleResponse insert(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return nationalHolidayService.insert(param, header);
  }

  @Override
  @POST
  @Path("/update")
  public SimpleResponse update(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
    return nationalHolidayService.update(param, header);
  }

  @Override
  @POST
  @Path("/inquiry")
  public SimpleResponse inquiry(Object param) {
    return nationalHolidayService.inquiry(param);
  }

  @Override
  @POST
  @Path("/entity")
  public SimpleResponse entity(Object param) {
    return nationalHolidayService.entity(param);
  }
}

