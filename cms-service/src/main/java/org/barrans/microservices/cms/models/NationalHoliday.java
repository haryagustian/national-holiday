package org.barrans.microservices.cms.models;

import com.barrans.util.CommonObjectActiveAndCreatedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "national_holiday")
public class NationalHoliday extends CommonObjectActiveAndCreatedDate implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @SequenceGenerator(name = "nationalHolidaySequence", sequenceName = "nationalHoliday_seq", allocationSize = 1, initialValue = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "nationalHolidaySequence")
  @Column(name = "id", nullable = false)
  public Long id;

  @Column(name = "date", nullable = false)
  public LocalDate date;

  @Column(name = "name", nullable = false)
  public String name;

  @Column(name = "status", nullable = false, length = 1)
  @NotNull(message = "Please provide a status")
  public Integer status;

}
