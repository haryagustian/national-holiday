package org.barrans.microservices.cms.services;

import com.barrans.util.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.quarkus.narayana.jta.runtime.TransactionConfiguration;
import org.barrans.microservices.cms.models.NationalHoliday;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class NationalHolidayService implements IAction {

  private static final Logger LOGGER = LoggerFactory.getLogger(NationalHolidayService.class.getName());

  @PersistenceContext
  EntityManager em;

  @Override
  @Transactional
  @TransactionConfiguration(timeout = 10)
  public SimpleResponse insert(Object param, String header) {
    try {
      ObjectMapper om = new ObjectMapper();
      om.registerModule(new JavaTimeModule());

      Map<String, Object> reqHeader = om.readValue(header, new TypeReference<>(){});

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.UNAUTHORIZED_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());

      NationalHoliday nh = om.convertValue(param, NationalHoliday.class);

      if (nh.date == null)
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "date is required", new HashMap<>());

      if (nh.name == null)
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "name is required", new HashMap<>());

//      nh.date = DateUtil.convertFromStringYYYYMMDDToLocalDate(nh.date.toString());
      nh.status = 0;
      ObjectActiveAndCreatedDateUtil.registerObject(nh, Long.parseLong(reqHeader.get("userId").toString()));
      nh.persist();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getNationalHoliday(nh.id));
    }catch (Exception e){
      LOGGER.error(e.getMessage(), e);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, new HashMap<>());
    }
  }

  @Override
  @Transactional
  @TransactionConfiguration(timeout = 10)
  public SimpleResponse update(Object param, String header) {
    try {
      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqHeader = om.readValue(header, new TypeReference<>(){});

      if (reqHeader.get("userId") == null || reqHeader.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.UNAUTHORIZED_CODE, GeneralConstants.UNAUTHORIZED, new HashMap<>());

      Map<String, Object> reqParam = om.convertValue(param, new TypeReference<>(){});

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new HashMap<>());


      NationalHoliday nh = NationalHoliday.findById(Long.parseLong(reqParam.get("id").toString()));

      if (nh == null)
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "national holiday does not exists", new HashMap<>());

      if (reqParam.get("date") != null)
        nh.date = DateUtil.convertFromStringYYYYMMDDToLocalDate(reqParam.get("date").toString());

      if (reqParam.get("name") != null)
        nh.name = reqParam.get("name").toString();

      ObjectActiveAndCreatedDateUtil.updateObject(nh, Long.parseLong(reqHeader.get("userId").toString()), true);
      nh.persist();

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getNationalHoliday(nh.id));
    }catch (Exception e){
      LOGGER.error(e.getMessage(), e);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, new HashMap<>());
    }
  }

  @Override
  public SimpleResponse inquiry(Object param) {
    try {
      ObjectMapper om = new ObjectMapper();
      Map<String, Object> reqParam = om.convertValue(param, new TypeReference<>(){});

      Integer limit = 5, offset = 0;
      Boolean filterDate = false, filterName = false;

      if (reqParam.containsKey("limit") && reqParam.get("limit") != null)
        limit = Integer.parseInt(reqParam.get("limit").toString());

      if (reqParam.containsKey("offset") && reqParam.get("offset") != null)
        offset = Integer.parseInt(reqParam.get("offset").toString());

      StringBuilder sb = new StringBuilder();
      sb.append("select nh.id, nh.date, nh.name from main.national_holiday nh where true");

      if (reqParam.containsKey("date") && reqParam.get("date") != null){
        sb.append(" and nh.date = :nhDate");
        filterDate = true;
      }

      if (reqParam.containsKey("name") && reqParam.get("name") != null){
        sb.append(" and nh.name ilike :nhName");
        filterName = true;
      }

      sb.append(" and nh.status = 0 order by nh.id desc");

      Query query = em.createNativeQuery(sb.toString());

      if (filterDate)
        query.setParameter("nhDate", reqParam.get("date").toString());

      if (filterName)
        query.setParameter("nhName", "%"+reqParam.get("name").toString()+"%");

      if (!limit.equals(-99) || !offset.equals(-99) || limit >= 1 || offset >= 0){
        query.setMaxResults(limit);
        query.setFirstResult(offset);
      }

      List<Object[]> resultQuery = query.getResultList();

      List<Map<String, Object>> data = BasicUtils.createListOfMapFromArrayWithPattern(resultQuery, "yyyy-MM-dd", "",  "id", "date", "name");

      Query queryCount = em.createNativeQuery(String.format(sb.toString().replaceFirst("select.* from", "select count (*) from").replaceFirst("order by .*", "")));

      for (Parameter<?> parameter : query.getParameters())
        queryCount.setParameter(parameter.getName(), query.getParameterValue(parameter.getName()));

      BigInteger count = (BigInteger) queryCount.getSingleResult();

      Map<String, Object> response = new HashMap<>();
      response.put("publicHolidays", data);

      Map<String, Object> metaData = new HashMap<>();

      metaData.put("totalPages", (int) Math.ceil(Double.parseDouble(count.toString()) / data.size()));
      metaData.put("total", count);
      metaData.put("page", data.size());

      response.put("meta", metaData);

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception e){
      LOGGER.error(e.getMessage(), e);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, new HashMap<>());
    }
  }

  @Override
  public SimpleResponse entity(Object param) {
    try {
      ObjectMapper om = new ObjectMapper();

      Map<String, Object> reqParam = om.convertValue(param, new TypeReference<>(){});

      if (reqParam.get("id") == null || reqParam.get("id").toString().equals(GeneralConstants.EMPTY_STRING))
        return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new HashMap<>());

      Map<String, Object> nh = getNationalHoliday(Long.parseLong(reqParam.get("id").toString()));

      if (nh.size() == 0)
        return new SimpleResponse(GeneralConstants.SUCCESS_CODE, "national holiday does not exists", new HashMap<>());

      Map<String, Object> response = new HashMap<>();
      response.put("national_holiday", nh);

      return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

    }catch (Exception e){
      LOGGER.error(e.getMessage(), e);
      return new SimpleResponse(GeneralConstants.FAIL_CODE, GeneralConstants.FAILED, new HashMap<>());
    }
  }

  public Map<String, Object> getNationalHoliday(Long id){
    NationalHoliday nh = NationalHoliday.findById(id);
    Map<String, Object> result = new HashMap<>();

    if (nh != null){
      result.put("id", nh.id);
      result.put("date", nh.date);
      result.put("name", nh.name);
    }

    return result;
  }
}

